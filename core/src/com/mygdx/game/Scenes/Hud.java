package com.mygdx.game.Scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.MyGdxGame;


public class Hud implements Disposable {

    public Stage stage;
    private Viewport viewport;


    private int worldTimer;
    private float timeCount;
    private static int score;

    private Label countdownLabel;
    private static Label scoreLabel;
    private Label timeLabel;
    private Label score2Label;

    public Hud(SpriteBatch sb){

        worldTimer = 300;
        timeCount = 0;
        score = 0;


        viewport = new FitViewport(MyGdxGame.V_WIDTH,MyGdxGame.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);


        Table table = new Table();
        table.top();
        table.setFillParent(true);


        countdownLabel = new Label(String.format("%03d", worldTimer), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        scoreLabel = new Label(String.format("%03d", score), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeLabel = new Label("TIME", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        score2Label = new Label("SCORE", new Label.LabelStyle(new BitmapFont(), Color.WHITE));



        table.add(timeLabel).expandX().padTop(10);
        table.add(score2Label).expandX().padTop(10);
        table.row();
        table.add(countdownLabel).expandX();
        table.add(scoreLabel).expandX();

        stage.addActor(table);

    }

    public void update(float dt){

        timeCount += dt;

        if(timeCount >=1){
            worldTimer--;
            countdownLabel.setText(String.format("%03d", worldTimer));
            timeCount = 0;
        }

    }

    public static void addScore(int value){
        score += value;
        scoreLabel.setText(String.format("%03d", score));
    }

    @Override
    public void dispose() {
        stage.dispose();

    }

}


